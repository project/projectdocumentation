# Contents of this file

* Introduction
* Requirements
* Recommended modules
* Installation
* Configuration
* Permissions
* Use
* Fields
* Troubleshooting
* Limitations
* Maintainer


# Introduction

The **Project Documentation** module creates a node content type
suitable for recording what projects are downloaded for use on a site
and metadata (what, when, why and who) them.

The use case for this project is to help site builders keep a “diary”
of the projects they install and why they did it (these things seems
to disappear in the mist of time as a website evolve).  It can also be
used to keep track om projects that has been evaluated and rejected,
to avoid making the same mistake twice.

To view the project documentation click on “Project list” in the
"Reports" section of the administrative GUI.

Not all features of the Drupal 7 version has been upgraded to the
Drupal 9/10 version.  Those not yet upgraded are marked "[TODO]".
They will all be in place for the first tagged release.

# Requirements

This module requires the following extensions installed:

* [Views][1]


# Recommended modules

* [Advanced Help][2]:  
  When this module is enabled, display of the project's `README.md`
  will be rendered when you visit
  `help/ah/projectdocumentation/README.md`.


# Installation

Install as you would normally install a contributed drupal
module. See: [Installing modules][3] for further information.


# Configuration

There is no configuration form.  Just enable the module and a content
type named “Project documentation” will appear along with a “Project
list” menu entry in "Reports" section of the administrative GUI.

Uninstalling the module will remove the content type, and the “Project
list” menu entry.

# Permissions

To access the project documentation, the user needs the permission
“Use the administration pages and help”.

To create project documentation nodes, the user needs the permission
“Project documentation: Create new content”. For full CRUD access also
give the user the other node permissions for project documentation.

# Use

There are two-ways to add documentation about the project's used on
your site:

1. Manually.
2. Semi-automatically [TODO]

To manually add a project, navigate to *Content » Add content »
Project documentation*, fill in the fields and press “Save” (as you
would normally when creating content on a Drupal site).

[TODO] To add undocumented projects semi-automatically, first clear
the cache (to rebuild the internal project list), then navigate to
*Config » System » Generate project documentation nodes*.  This will
scan your site for projects and report on their status. Pressing the
“Create documenta&shy;tion&nbsp;…”-button will bulk-create new nodes
for the undocumented projects listed in the summary.  It will not
touch projects already documented.  It will not add projects where the
project key is missing (see [Troubleshooting](#trouble) below).

[TODO] The module provides a quick and dirty mechanism (see
[Limitations](#limits) below) for exporting and importing the reasons
for using a project.  You may export the short name and the
“Why”-component of the projects documented to a CSV-file using
semicolons (<code>;</code>) as delimiters.  To do this, navigate to
*Config » System » Generate project documentation nodes* and press the
“Export CSV”-button.  You may copy the CSVs that appear on this page.
To import it into another site, first generate the documentation for
the site semi-automatically.  Then navigate back to *Config » System »
Generate project documentation nodes* and select the “Import CSV”-tab.
Paste the CSV you just copied into the form and press
“Import”. **Note:** this will *overwrite* the “Why”-component for all
projects whose short name appear in the CSV-file.  Therefore, it
should *only* be used initially, to import some standard reasons you
have for using specific projects.

# Fields

Two pre-existing fields are appropriated and used:

* Project name  `[title]`
* Why `[body]`

Four additional fields are created:

* Shortname `[field_projdoc_shortname]`
* Link to project at Drupal.org `[field_projdoc_projectpage]`
* Date first installed `[field_projdoc_dateinst]`
* Owner of project `[field_projdoc_who]`

[TODO] The module will abort installation if one if these four
additional fields already exists on the site.

# Troubleshooting

You may encounter the error messages and warnings similar to these:

* The module could not be enabled. The following field already exists:
&hellip;  
If this error occurs when you try to install the module, some other
project interferes with this module's field namespace, or the module
has not been properly uninstalled.

* [TODO] File `my_project.info.yml` for “My project” is missing the
“project” key.
The “project” key is normally added by packaging script at
Drupal.org. Its value is the project's short name.  If this is
missing, the project originated from some other source.  You may add
the project manually, or add the key to `my_project.info.yml`.


# Limitations

The module relies on the “project” key in the project's
`.info.yml`-files to correctly identify the project a particular
module belongs to. If this key is missing, a warning is printed when
projects are scanned for inclusion in the project documentation. To
fix this, see [Troubleshooting](#trouble) above.

If the “Why”-component contains a semicolon, it will be silently
converted to a comma upon export.  This will also effect HTML
entities, such as “`&amp;`”.

# Maintainer

The project is created and maintained by [Gisle Hannemyr][4].

Any help with development (patches, reviews, comments) are welcome.

Development has been sponsored by [Hannemyr Nye Medier AS][5].

[1]: https://www.drupal.org/project/views
[2]: https://www.drupal.org/project/advanced_help
[3]: https://drupal.org/documentation/install/modules-themes/modules-7
[4]: https://www.drupal.org/u/gisle
[5]: https://hannemyr.no/
