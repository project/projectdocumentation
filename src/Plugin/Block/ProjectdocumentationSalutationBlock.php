<?php

namespace Drupal\projectdocumentation\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\projectdocumentation\Services\ProjectdocumentationSalutation;

/**
 * Project Documentation Salutation block.
 *
 * @Block(
 *  id = "projectdocumentation_salutation_block",
 *  admin_label = @Translation("Project documentation salutation"),
 * )
 */
class ProjectdocumentationSalutationBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The salutation service.
   *
   * @var \Drupal\projectdocumentation\Services\ProjectdocumentationSalutation
   */
  protected $salutation;

  /**
   * Constructs a ProjectdocumentationSalutationBlock.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\projectdocumentation\Services\ProjectdocumentationSalutation $salutation
   *   The salutation service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ProjectdocumentationSalutation $salutation) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->salutation = $salutation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('projectdocumentation.salutation')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#markup' => $this->salutation->getSalutation(),
    ];
  }

}
