<?php

namespace Drupal\projectdocumentation\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\projectdocumentation\Services\ProjectdocumentationSalutation;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for the salutation message.
 */
class ProjectdocumentationController extends ControllerBase {

  /**
   * The salutation service.
   *
   * @var \Drupal\projectdocumentation\\Services\ProjectdocumentationSalutation
   */
  protected $salutation;

  /**
   * ProjectdocumentationController constructor.
   *
   * @param \Drupal\projectdocumentation\Services\ProjectdocumentationSalutation $salutation
   */
  public function __construct(ProjectdocumentationSalutation $salutation) {
    $this->salutation = $salutation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('projectdocumentation.salutation')
    );
  }

  /**
   * projectdocumentation
   *
   * @return array
   *   Our message.
   */
  public function projectdocumentation() {
    return [
      '#markup' => $this->salutation->getSalutation(),
    ];
  }

}
